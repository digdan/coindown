<?php
define('SITEKEY', 'mr5BO35QNmFTk4J9223u6PAcKY14474hw');
define('APP_BASE',__DIR__);
return array(
	"dsn"=>"mongodb://localhost",
	"meta"=>array(
		"title"=>"Coin Down - The Crypto-currency Bitcoin Stock Price Alert Service System"
	),
	"ttl"=>array(
		"email_links"=>(12*60*60)
	),
	"paths"=>array(
		"templates"=>APP_BASE."/www/templates/",
	),
	"bot"=>array(
		"signature"=>"CoinCoinBot ( http://www.coindown.com/bot )",
		"increment"=>300,
	),
	'targets'=>array(
			'btc/usd'=>'Bitcoin',
			//'ltc/usd'=>'Litecoin'
	),
	'increments'=>array(
		300=>'5 minutes',
		900=>'15 minutes',
		1800=>'30 minutes',
		3600=>'1 hour',
		10800=>'3 hours',
		86400=>'1 day',
		604800=>'1 week'
	),
	'valueTypes'=>array(
		'points',
		'percent'
	),
	'directions'=>array(
		'up',
		'down'
	)
);
?>
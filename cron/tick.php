#!/usr/bin/php -q
<?php
	$start = microtime(true); //Track Script Execution Time

	include(__DIR__."/cronfig.php"); //Load Cron based config files
	include(__DIR__."/lib/ticker.php");
	include(__DIR__."/lib/summary.php");
	include(__DIR__."/lib/buckets.php");
	include(__DIR__."/lib/email.php");
	include(__DIR__."/lib/tasker.php");

	$m = new Mongo($config['dsn']); //DB Connect
	$db = $m->coindown; //Choose our DB Source
	$ticker = $db->ticker; //Choose the collection
	$cache = $db->summary;
	$users = $db->users;
	$rules = $db->rules;


	$t = new Ticker(); //Build up new ticker
	$t->tick($ticker); //Tick with new ruleset

	$summary = new Summary(); //Build up new Summary
	$summary->readPoints($ticker); //Summarize read points
	foreach ($config['targets'] as $target=>$targetName) $summary->calcDiffs($target); //Find the differences from last tick
	foreach ($config['increments'] as $increment=>$incrementName) { //Iterate through targetted increments
		foreach ($config['targets'] as $target=>$targetName) { //Iterate through Targets
			$data[$target][$increment] = $summary->diff($target, $increment); //Create a summary of differences from last tick
		}
	}

	$data['recorded'] = time(); //Add timestamp
	$cache->remove(); //Remove previous summary
	$cache->insert($data); //Add new Summary

	$summary = $cache->findOne(); //Load Summary

	//Build Bucket Matrix
	foreach($config['targets'] as $target=>$targetName) { //Iterate through buckets
		$target_parts = explode("/",$target);
		foreach($config['increments'] as $increment=>$incrementName) {
			$marks = $summary[$target][$increment]; //Points and Percs
			foreach($config['valueTypes'] as $valueType) {
				foreach(array('up','down') as $direction) {
					$emails = array(); //Clear email buffer
					$filter = ($direction == "up" ? "\$lt" : "\$gt");
					$bucket = Buckets::name($target_parts[0], $direction, $increment, $valueType);
					$search = array(
						'bucket' => $bucket,
						'value' => array($filter => $marks[$valueType])
					);
					$found = $rules->find($search);
					if ($found->count() > 0) {
						foreach($found as $record) {
							$emails[] = $record['email'];
						}
						$email = Email::build($target_parts[0], $direction, $incrementName, $valueType, $marks);
						//Tasker::task('email', $email, $emails);
					}
				}
			}
		}
	}


	$end = round(microtime(true) - $start,4);

	echo "Tick took {$end} miliseconds.\n";

?>

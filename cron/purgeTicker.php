#!/usr/bin/php -q
<?php
	include("cronfig.php");
	include(__DIR__."/lib/ticker.php");
	include(__DIR__."/lib/summary.php");
	include(__DIR__."/lib/buckets.php");
	include(__DIR__."/lib/email.php");
	include(__DIR__."/lib/tasker.php");
	$m = new Mongo($config['dsn']);
	$db = $m->coindown;
	$collection = $db->ticker;
	echo "Removing ticker history.\n";
	$collection->remove();
?>

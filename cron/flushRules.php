#!/usr/bin/php -q
<?php
	include("cronfig.php");
	include(__DIR__."/lib/ticker.php");
	include(__DIR__."/lib/summary.php");
	include(__DIR__."/lib/buckets.php");
	include(__DIR__."/lib/email.php");
	include(__DIR__."/lib/tasker.php");

	$m = new Mongo($config['dsn']);
	$db = $m->coindown;
	$collection = $db->ticker;
	$cache = $db->summary;
	$users = $db->users;
	$rules = $db->rules;

	echo "Removing previous rules.\n";
	$rules->remove();
	/*
	echo "Adding random ruleset.";
	for($i=0;$i<=1000000;$i++) {
		$ruleset = array('bucket' => Buckets::random(), 'value' => round((rand(0, 200) / 100), 2), 'email' => 'dan@danmorgan.net');
		$rules->insert($ruleset);
		echo ".";
	}
	echo "\n";
	*/
	echo "Done.\n";
?>

#!/usr/bin/php -q
<?php
	include("cronfig.php");
	include(__DIR__."/lib/ticker.php");
	include(__DIR__."/lib/summary.php");
	include(__DIR__."/lib/buckets.php");
	include(__DIR__."/lib/email.php");
	include(__DIR__."/lib/tasker.php");

	$m = new Mongo($config['dsn']);
	$db = $m->coindown;
	$ticker = $db->ticker;
	$cache = $db->summary;
	$users = $db->users;
	$rules = $db->rules;

	$summary = new Summary();
	$summary->readPoints($ticker);
	foreach($config['targets'] as $target=>$targetName) $summary->calcDiffs($target);
	foreach($config['increments']as $increment=>$incrementName) {
		foreach($config['targets'] as $target=>$targetName) {
			$data[$target][$increment] = $summary->diff($target,$increment);
		}
	}

	$data['recorded'] = time();
	$cache->remove();
	$cache->insert($data);

?>

#!/usr/bin/php -q
<?php
//Expired can delete this script - now included in tick script
include("cronfig.php");
include(__DIR__ . "/lib/ticker.php");
include(__DIR__."/lib/summary.php");
include(__DIR__."/lib/buckets.php");
include(__DIR__."/lib/email.php");
include(__DIR__."/lib/tasker.php");

$m = new Mongo(MONGODSN);
$db = $m->coindown;
$collection = $db->ticker;
$cache = $db->summary;
$users = $db->users;
$rules = $db->rules;

$summary = $cache->findOne();

//Build Bucket Matrix
foreach($targets as $target) {
	$target_parts = explode("/",$target);
	foreach($increments as $increment=>$incrementName) {
		$marks = $summary[$target][$increment]; //Points and Percs
		foreach($valueTypes as $direction=>$valueType) {
			$filter = ($direction == "up" ? "\$lt" : "\$gt");
			$bucket = Buckets::name($target_parts[0],$direction,$increment,$valueType);
			$found = $rules->find(array('bucket'=>$bucket,'value'=>array($filter=>$marks['points'])));
			$email = Email::build($target_parts[0],$direction,$incrementName,$valueType,$marks);
			Tasker::task('email',$email,$found);
		}
	}
}
?>
#!/usr/bin/php -q
<?php
	include("cronfig.php");
	include(__DIR__ . "/lib/ticker.php");
	include(__DIR__."/lib/summary.php");

	$m = new Mongo(MONGODSN);
	$db = $m->coindown;

	$collection = $db->ticker;
	$collection->ensureIndex(array('recorded'=>1));

	$summary = $db->summary;

	$users = $db->users;
	$users->ensureIndex(array('email'=>1,'password'=>1));

	$rules = $db->rules;
	$rules->ensureIndex(array('bucket'=>1,'value'=>1));
?>
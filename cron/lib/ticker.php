<?php
	class Ticker {
		function tick($collection) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "http://api.coindesk.com/v1/bpi/currentprice/USD.json");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$rawData = curl_exec($curl);
			curl_close($curl);

			// decode to array
			$data = json_decode($rawData);
			unset($data->disclaimer);

			//connect to mongodb
			$data->recorded = time();
			$data->id = 'btc/usd';
			$collection->insert($data);
		}
	}
?>
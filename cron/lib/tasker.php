<?php
	class Tasker {
		static function task($action,$data,$list) {
			switch ($action) {
				case 'email' :
					echo "Sending alerts to ".count($list)." subscribers.\n";
					foreach($list as $address) mail($address,$data['subject'],$data['text']);
					break;
			}
		}
	}
?>
<?php
class Email {
	static function build($coin,$direction,$incrementName,$valueType,$value) {
		return array(
			'subject'=>"CoinDown Alert : {$coin} {$direction} {$value} {$valueType}",
			'text'=>"Alert! CoinDown has detected {$coin} has moved {$direction} {$value} {$valueType} in the last {$incrementName}",
			'html'=>"Alert! CoinDown has detected {$coin} has moved {$direction} {$value} {$valueType} in the last {$incrementName}"
		);
	}
}
?>
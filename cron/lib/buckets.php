<?php
	class Buckets {
		static function name($coinType='btc',$direction='down',$timeframe=300,$pointType='points') {
			return strtoupper("{$coinType}.{$direction}.{$pointType}.{$timeframe}");
		}
		static function shuffle_assoc(&$array) {
			$keys = array_keys($array);
			shuffle($keys);
			foreach($keys as $key) $new[$key] = $array[$key];
			$array = $new;
			return true;
		}
		static function random() { //Returns a random bucket, for benchmarking.
			global $targets,$increments;
			shuffle($targets);
			$target = $targets[0];
			$target_parts = explode("/",$target);
			$coinType = $target_parts[0];
			self::shuffle_assoc($increments);
			foreach($increments as $increment=>$incrementName) continue;
			if (rand(0,2) > 1) {
				$dir = "down";
			} else {
				$dir = "up";
			}
			if (rand(0,2) > 1) {
				$valType = "points";
			} else {
				$valType = "percent";
			}
			$bucketName = strtoupper("{$coinType}.{$dir}.{$valType}.{$increment}");
			return $bucketName;
		}
	}
?>
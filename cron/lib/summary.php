<?php

/**
 * Class Summary
 *
 *  Summarizes ticker information into percentage and point changes.
 */
	class Summary {
		var $points;
		var $diff_points;
		var $diff_percs;
		var $highLow;

		private function setPoint($coinType,$recorded,$value) {
			$this->points[$coinType][$recorded] = $value;
		}
		public function readPoints(MongoCollection $ticker) {
			$threshold = time() - (60 * 60 * 24 * 7); //1 Week
			$records = $ticker->find(array('recorded'=>array('$gt'=>$threshold)));
			foreach($records as $record) {
				$recorded = $record['recorded'];
				if (!isset($record['id'])) $record['id'] = 'btc/usd';
				unset($record["_id"]);
				unset($record['recorded']);
				$this->setPoint($record['id'],$recorded,$record['bpi']['USD']['rate']);
			}
		}
		public function calcDiffs($coinType) {
			if (isset($this->points[$coinType]) and is_array($this->points[$coinType])) {
				$lastPoint = false;
				foreach($this->points[$coinType] as $recorded=>$point) {
					$diff = 0;
					if ($lastPoint === FALSE) { //First point
						$lastPoint = $point;
					}
					$diff = ($lastPoint - $point);
					$this->diff_points[$coinType][$recorded] = $diff; //Point Difference
					$this->diff_percs[$coinType][$recorded] = round(($diff / $point)*100,4); //Percentage Difference
					$lastPoint = $point;
				}
			}
			return true;
		}

		public function diff($coinType,$timeFrame=300) {
			$config = Config::instance();
			$slots = round($timeFrame / $config['bot']['increment'])+1;

			//Points
			$pointsArray = array_splice($this->diff_points[$coinType],0,$slots);
			$points = array_sum($pointsArray);


			//Percs
			$percsArray = array_splice($this->diff_percs[$coinType],0,$slots);
			$percs = array_sum($percsArray);
				

			//HiLow
			$hiLowArray = array_splice($this->points[$coinType],0,$slots);
			$high = max($hiLowArray);
			$low = min($hiLowArray);

			return array('points'=>$points,'percent'=>$percs,'high'=>$high,'low'=>$low);
		}

	}
?>

<?php include("header.php"); ?>

<div class="page-container">
    <div class="page-content">
        <div class="page-content-wrap">
            <div class="page-content-holder">
                <div class="row">
                    
                    <!-- START DEFAULT WIZARD -->
                    <div class="panel panel-default">

                        <div class="panel-body">   
                            <div class="wizard">

                                <h3>Setup Your Free Alert</h3>

                                <ul class="anchor">
                                    <li class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                        <a href="#step-1" class="selected" isdone="1" rel="1">
                                            <span class="stepNumber">1</span>
                                            <span class="stepDesc">Configure Alert!<br><small>Set Your Thresholds</small></span>
                                        </a>
                                    </li>
                                    <li class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                        <a href="#step-2" class="disabled" isdone="0" rel="3">
                                            <span class="stepNumber">2</span>
                                            <span class="stepDesc">Your Details<br><small>Who Are You?</small></span>                   
                                        </a>
                                    </li>
                                    <li class="col-md-4 col-sm-6 col-xs-12 no-padding">
                                        <a href="#step-3" class="disabled" isdone="0" rel="4">
                                            <span class="stepNumber">3</span>
                                            <span class="stepDesc">Confirmation<br><small>Does everything look right?</small></span>                   
                                        </a>
                                    </li>
                                </ul>



                                <div class="stepContainer">

                                    <!-- Step 1 -->
                                    <div id="step-1" class="content" style="display: block;">   
                                        <form class="form-inline col-sm-10 col-sm-offset-1">
                                            <div class="form-group">
                                                
                                                <span class="alert-builder">CoinDown! Alert me via
                                                    <select>
                                                        <option>Email</option>
                                                        <option>SMS</option>
                                                    </select>
                                                    when
                                                    <select>
                                                        <option>BTC</option>
                                                        <option>LTC</option>
                                                    </select>
                                                    changes more than
                                                    <select>
                                                        <option>2%</option>
                                                        <option>6%</option>
                                                        <option>10%</option>
                                                    </select>
                                                    in
                                                    <select>
                                                        <option>5 Minutes</option>
                                                        <option>15 Minutes</option>
                                                        <option>30 Minutes</option>
                                                    </select>
                                                    </span>
                                            </div>
                                        </form>                                         
                                        
                                    </div>

                                    <!-- Step 2 -->
                                    <div id="step-2" class="content" style="display: none;">
                                        <form class="form-horizontal text-left">
                                            <div class="form-group">
                                                <label for="UserName" class="col-md-2 control-label">Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="UserName" placeholder="Your Name...">
                                                </div>                                   

                                                <label for="UserEmail" class="col-md-1 control-label">Email</label>
                                                <div class="col-md-4">
                                                    <input type="email" class="form-control" id="UserEmail" required placeholder="Your Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="UserCountryCode" class="col-md-2 control-label">Phone</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" id="UserCountryCode" placeholder="+44">                              
                                                </div>
                                                <div class="col-md-2"> 
                                                    <input type="tel" class="form-control" id="UserPhone" placeholder="Your Phone Number">
                                                </div>
                                            </div>
                                        </form>                              
                                    </div>

                                    <div id="step-3" class="content" style="display: none;">
                                        <div class="col-sm-11 col-sm-offset-1 alert-preview">
                                            <p><strong>Echo Alert</strong></p>
                                            <p><strong>Detect Premium Settings</strong></p>
                                            <p><strong>If Premium Settings Display Paypal</strong></p>
                                        </div>
                                    </div>

                                </div>

                            </div>                                       
                            <!-- END DEFAULT WIZARD -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("footer.php"); ?>
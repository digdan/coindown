<!-- page footer -->
<div class="page-footer">

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-dark-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder page-footer-holder-main">

            <div class="row">

                <!-- about -->
                <div class="col-md-3">
                    <h3>Partners</h3>
                    <p>Contact us to talk about partnering with CoinDown.</p>
                </div>
                <!-- ./about -->

                <!-- quick links -->
                <div class="col-md-3">
                    <h3>Quick links</h3>

                    <div class="list-links">
                        <a href="#">Home</a> 
                        <a href="#" data-toggle="modal" data-target="#AccountLogin">Login</a>
                        <a href="#">News & Blog</a>
                        <a href="#" data-toggle="modal" data-target="#Contact">Contact Us</a>
                    </div>                                
                </div>
                <!-- ./quick links -->

                <!-- recent tweets -->
                <div class="col-md-3">
                    <h3>Recent Tweets</h3>

                    <div class="list-with-icon small">
                        <div class="item">
                            <div class="icon">
                                <span class="fa fa-twitter"></span>
                            </div>
                            <div class="text">
                                <a href="#">@AntBentinck</a> CoinDown keeps me updated on prices so I know exactly when to by Bitcoins! #Awesome
                            </div>
                        </div>
                    </div>

                </div>
                <!-- ./recent tweets -->

                <!-- contacts -->
                <div class="col-md-3">
                    <h3>Newsletter & Offers</h3>
                    <div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Your email"/>
                        <div class="input-group-btn">
                            <button class="btn btn-primary"><span class="fa fa-paper-plane"></span></button>
                        </div>
                    </div>                                   
                    </div>



                </div>
                <!-- ./contacts -->

            </div>

        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

    <!-- page footer wrap -->
    <div class="page-footer-wrap bg-darken-gray">
        <!-- page footer holder -->
        <div class="page-footer-holder">
            

            <!-- copyright -->
            <div class="copyright col-md-4 col-sm-4 col-xs-12">
               <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script> CoinDown - All Rights Reserved                            
            
            <!-- legal links -->
            <div class="legal-links">
                <p>
                    <a href="/terms-of-service">Terms of Service</a>
                    <a href="/privacy-policy">Privacy Policy</a>
                </p>    
            </div>
            <!-- ./legal links -->
            
            </div>
            <!-- ./copyright -->

            
            <!-- social links -->
            <div class="social-links col-md-2 col-sm-2 col-xs-12">
                <a href="#"><span class="fa fa-facebook"></span></a>
                <a href="#"><span class="fa fa-twitter"></span></a>
            </div>                        
            <!-- ./social links -->

        </div>
        <!-- ./page footer holder -->
    </div>
    <!-- ./page footer wrap -->

</div>
<!-- ./page footer -->

</div>        
<!-- ./page container -->

</body>
</html>
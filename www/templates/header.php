<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META Section -->
        <title>CoinDown - Crypto Currency Price Monitor</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!-- END META Section -->

        <link rel="stylesheet" type="text/css" href="assets/css/theme-default.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" media="screen" />

	    <!-- page scripts -->
	    <script type="text/javascript" src="assets/js/plugins/jquery/jquery.min.js"></script>
	    <script type="text/javascript" src="assets/js/plugins/jquery/jquery-ui.min.js"></script>
	    <script type="text/javascript" src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>

	    <script type="text/javascript" src="assets/js/plugins/mixitup/jquery.mixitup.js"></script>
	    <script type="text/javascript" src="assets/js/plugins/appear/jquery.appear.js"></script>

	    <script type="text/javascript" src="assets/js/plugins/revolution-slider/jquery.themepunch.tools.min.js"></script>
	    <script type="text/javascript" src="assets/js/plugins/revolution-slider/jquery.themepunch.revolution.min.js"></script>

	    <script type="text/javascript" src="assets/js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
	    <script type="text/javascript" src="assets/js/plugins/jquery-validation/jquery.validate.js"></script>

	    <script type="text/javascript" src="assets/js/actions.js"></script>
	    <script type="text/javascript" src="assets/js/slider.js"></script>
	    <script type="text/javascript" src="assets/js/plugins.js"></script>

	    <script src="https://apis.google.com/js/platform.js" async defer></script>

	    <!-- ./page scripts -->

        <!-- Gooogle Analytics Tracking -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-46901182-10', 'auto');
          ga('send', 'pageview');
        </script>
        <!-- END Google Analytics -->

    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=1569719786573015&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- page container -->
        <div class="page-container">

            <!-- page header -->
            <div class="page-header">

                <!-- page header holder -->
                <div class="page-header-holder">

                    <!-- page logo -->
                    <div class="logo">
                        <a href="/"><img title="CoinDown" alt="CoinDown" src="assets/images/CoinDown.svg"></a>
                    </div>
                    <!-- ./page logo -->

                    <!-- nav mobile bars -->
                    <div class="navigation-toggle">
                        <div class="navigation-toggle-button"><span class="fa fa-bars"></span></div>
                    </div>
                    <!-- ./nav mobile bars -->

                    <!-- social tools -->
                    <div class="social-tools">
                        <ul class="tools-stacked">
                            <li>
                                <div class="fb-like" data-href="https://coindown.com" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                            </li>
                            <li>
                                <div class="g-plusone" data-size="medium" data-href="https://coindown.com"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- ./social tools -->

                    <!-- navigation -->
                    <ul class="navigation">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#WhatsCoinDown">What's This?</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#Contact">Need Help?</a>
                        </li>
                        <!-- ./navigation -->                                             

                        <!-- Modal Login -->
                        <div class="modal fade" id="AccountLogin" tabindex="-1" role="dialog" aria-labelledby="AccountLogin" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="AccountLogin">CoinDown Login <small> (Premium Accounts Only)</small></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-signin">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Email" required autofocus>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" placeholder="Password" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="pull-left">
                                                    <input type="checkbox" value="remember-me">
                                                    Remember me
                                                </label>
                                                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Modal Contact -->
                        <div class="modal fade" id="Contact" tabindex="-1" role="dialog" aria-labelledby="Contact" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="Contact">Contact Us</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" action="" method="post" >
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="InputName">Your Name</label>
                                                <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Name" required>
                                            </div>
                                                <div class="form-group">
                                              <label for="InputEmail">Your Email</label>
                                                <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" required  >
                                                </div>
                                                <div class="form-group">
                                              <label for="InputMessage">Message</label>
                                                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                                            </div>
                                                <div class="form-group">
                                              <label for="InputReal">What is 4+3? (Simple Spam Checker)</label>
                                                <input type="text" class="form-control" name="InputReal" id="InputReal" required>
                                               </div>
                                          </div>
                                        </form>                                  
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>     
                        
                        
                        <!-- Whats CoinDown? -->
                        <div class="modal fade" id="WhatsCoinDown" tabindex="-2" role="dialog" aria-labelledby="WhatsCoinDown" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="AccountLogin">What's CoinDown?</h4>
                                    </div>
                                    <div class="modal-body">

                                    </div>
                                </div>
                            </div>
                        </div>                           

                </div>
                <!-- ./page header holder -->

            </div>
            <!-- ./page header -->
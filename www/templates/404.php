<?php include("header.php") ?>
	<section class="contact section has-pattern">
		<div class="container">
			<div class="contact-inner">
				<div class='text-center'>
					<H1>404 Page Not Found!</H1>
                                        <p><strong>Sorry the page you are looking for cannot be found on our servers...</strong></p>
                                        <iframe src="//player.vimeo.com/video/38195013?title=0&byline=0&portrait=0" width="500" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
			</div>
		</div>
	</section>
<?php include("footer.php") ?>
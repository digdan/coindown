<?php
return array(
	'index'=>array("GET|POST","/","controllers_Main#main"),
	'auth'=>array("GET|POST","/auth/[*:id]/[*:token]","controllers_Auth#auth"),
        'news'=>array("GET|POST","/news","controllers_Static#display"),
        'terms'=>array("GET|POST","/terms-of-service","controllers_Static#display"),
        'privacy'=>array("GET|POST","/privacy-polcy","controllers_Static#display"),
        'alerts'=>array("GET|POST","/alerts","controllers_Static#display"),    
        'news'=>array("GET|POST","/news","controllers_Static#display"),
        'alerts'=>array("GET|POST","/alerts","controllers_Static#display"),
	'settings'=>array("GET|POST","/settings","controllers_Settings#main"),
	//AJAX
	'ajaxRecent'=>array("GET|POST","/ajaxRecent","controllers_Main#recent")
);
?>

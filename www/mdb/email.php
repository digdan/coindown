<?php
class mdb_email extends DBObject {
	const collectionName = 'emails';
	const use_random_id = true;

	const STATUS_UNVERIFIED = 0;
	const STATUS_VERIFIED = 1;

	var $email;
	var $nonce;
	var $created;
	var $verified;
	var $status;

	function __construct($id = NULL) {
		if (is_null($id)) { //New Record
			$this->created = time();
			$this->reNonce();
		}
	}

	function checkNonce($nonce) {
		return Nonce::check(SITEKEY,$nonce);
	}

	function reNonce() {
		$config = Config::instance();
		$this->nonce = Nonce::generate(SITEKEY,$config['ttl']['email_links']);
	}

}
?>
<?php
	class mdb_rule extends DB_Object {
		const collectionName = 'emails';
		const use_random_id = true;

		var $bucket;
		var $value;
		var $email;

		static function randomBucket() { //Returns a random bucket, for benchmarking.
			$config = Config::instance();
			$targets = $config['targets'];
			$increments = $config['increments'];
			$valTypes = $config['valueTypes'];
			shuffle($targets);
			$target = $targets[0];
			$target_parts = explode("/",$target);
			$coinType = $target_parts[0];
			self::shuffle_assoc($increments);
			foreach($increments as $increment=>$incrementName) continue;
			if (rand(0,2) > 1) {
				$dir = "down";
			} else {
				$dir = "up";
			}

			shuffle($valTypes);
			$valType = $valTypes[0];

			$bucketName = strtoupper("{$coinType}.{$dir}.{$valType}.{$increment}");
			return $bucketName;
		}

	}
?>
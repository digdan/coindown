<?php
	class mdb_GIF extends DBObject {
		const collectionName = 'gifs';
		const use_random_id = true;
		const prefix='gifbpm-';
		const prefixCover='gifbpmc-';

		var $url; //Original URL
		var $frames; //# of frames
		var $size; //Size in bytes;
		var $duration; //Duration in MS
		var $bpms; //Array of potential BPMs
		var $info; //All info
		var $cover; //Binary info of single frame of GIF
		var $created;

		private $file;


		function fetch($url=NULL,$force=false) {
			if (is_null($url)) {
				$url = $this->url;
			}
			if ((self::count(['url'=>$url]) > 0) and ($force === false)) { //Record exists
				$res = self::searchOne(['url'=>$url]);
				$this->get($res->_id);
				return true;
			}

			$this->created = time();

			if ($this->isValid($url)) {
				if ($this->download($url)) {
					if ($this->getSize()) {
						if ($this->getDuration()) {
							if ($this->getCover()) {
								if ($this->getBPMs()) {
									return true;
								} else {
									echo "Error BPMING";
								}
							} else {
								echo "Error Covering.";
							}
						} else {
							echo "Error Durating.";
						}
					} else {
						echo "Error Sizing.";
					}
				} else {
					echo "Error Downloading.";
				}
			} else {
				echo "Invalid URL";
			}
			return false;
		}

		function isValid($url) { //Validates a remote URL as a GIF
			if (!$fp = curl_init($url)) return false;
			if(!($fh = @fopen($url, 'rb')))
				return false;
			$count = 0;
			while(!feof($fh) && $count < 2) {
				$chunk = fread($fh, 1024 * 100); //read 100kb at a time
				$count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
			}
			fclose($fh);
			return $count > 1;
		}


		function download($url) {
			$config = Config::instance();
			$file = tempnam($config['bot']['temp'],static::prefix);
			$this->file = $file;
			$fp = fopen ( $file , 'w+');
			$ch = curl_init(str_replace(" ","%20",$url));//Here is the file we are downloading, replace spaces with %20
			curl_setopt($ch, CURLOPT_TIMEOUT, 50);
			curl_setopt($ch, CURLOPT_USERAGENT, $config["bot"]["signature"]);
			curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_NOPROGRESS, false);
			curl_exec($ch); // get curl response
			curl_close($ch);
			fclose($fp);
			$this->url = $url;
			return true;
		}

		public function getSize() {
			$this->size = getimagesize($this->file);
			return true;
		}

		public function getDuration() {
			$defaultRate = 10;
			// see http://www.w3.org/Graphics/GIF/spec-gif89a.txt for more info
			$gif_graphic_control_extension = "/21f904[0-9a-f]{2}([0-9a-f]{4})[0-9a-f]{2}00/";
			$file = file_get_contents($this->file);
			$file = bin2hex($file);

			// sum all frame delays
			$total_delay = 0;
			$frames = 0;
			preg_match_all($gif_graphic_control_extension, $file, $matches);
			foreach ($matches[1] as $match) {
				// convert little-endian hex unsigned ints to decimals
				$delay = hexdec(substr($match,-2) . substr($match, 0, 2));
				if ($delay == 0) $delay = $defaultRate; //Default frame rate
				$total_delay += $delay;
				$frames++;
			}

			// delays are stored as hundredths of a second, lets convert to seconds
			//$total_delay /= 100;

			$this->frames = $frames;
			if ($total_delay == 0) $total_delay = (($frames + 1) * $defaultRate);
			$this->duration = $total_delay;
			return true;
		}

		public function getCover() {
			$config = Config::instance();
			$image = new Imagick($this->file);
			$image = $image->coalesceImages();
			foreach ($image as $frame) {
				$frame->cropThumbnailImage($config['bot']['preview'][0], $config['bot']['preview'][1]);
				break;
			}
			$file = tempnam($config['bot']['temp'],static::prefixCover);
			$fp = fopen ( $file , 'wb');
			fputs($fp,$frame->getImageBlob());
			fclose($fp);
			$this->cover = FKeeper::instance()->keep($file);
			unlink($file);
			return true;
		}


		public function getBPMs() {
			$duration = $this->duration;
			$bpm = (6000 / $duration );
			if ($bpm >= 100) $bpm = round($bpm / 2);
			if ($bpm <= 50) $bpm *=2;
			if ($bpm <= 0) return false;

			$bpms[] = round(($bpm / 2),2);
			$bpms[] = round($bpm,2);
			$bpms[] = round(($bpm * 2), 2);

			$this->bpms = $bpms;

			return true;
		}

	}
?>

<?php
	class controllers_Global {
		function main($route,$params) {
			global $coindown;
			$scope = Scope::instance();
			$scope->assign( "route",$route );
			$scope->assign( "routeSlug",Utils::slugify($route) );
			$scope->assign( "emailAuth", (isset($_SESSION['email_auth']) ? $_SESSION['email_auth'] : FALSE ));
			$scope->assign('params',$params);
			$meta = array(
				'title'=>'Coin Down - Automated CryptoCurrency Bitcoin Litecoin Alert System',
				'description'=>'Create custom personalized alerts on changes to various cryptocurrencies such as bitcoin and litecoin.',
			);
			$scope->assign('meta',$meta);
			$scope->assign('config',Config::instance());
		}
	}
?>
<?php
	class controllers_Auth {

		function auth($params,$route) {
			$auth = false;
			$s = Scope::instance();
			$id = (isset($params['id']) ? $params['id'] : NULL);
			$token = (isset($params['token']) ? $params['token'] : NULL);
			if (isset($id) and (isset($token))) { //Attempting to auth
				$suspect = new mdb_email($id);
				$auth = $suspect->checkNonce($token);
			}
			if ($auth) {
				//Redirect to controll panel
				$_SESSION['email_auth'] = $suspect->_id;
				header("Location: ".Router::instance()->generate('settings'));
				die();
			} else { //No such luck
				echo $s->render('badAuth');
			}
		}

	}
?>
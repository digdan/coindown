<?php
class controllers_Main {
	function main($params,$route) {
		$s = Scope::instance();
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		}
		echo $s->render($route);
	}

	function test($params,$route) {
		$e = new EchoNest();
		$e->getBPM(75);

	}

	function recent($params,$route) {
		global $coindown;
		$config = Config::instance('default');
		$summary = $coindown->summary->findOne();
		foreach($config['targets'] as $coin=>$coinName) {
			echo "<fieldset style='margin:20px;'><legend>{$coinName} USD</legend>";
			foreach($config['increments'] as $inc=>$incName) {
				if ($inc == 300) continue; // No 5 minute updates pls
				$markPerc = round($summary[$coin][$inc]['percent'], 4);
				if ($markPerc == 0) $markClass = 'text-default';
				if ($markPerc < 0) {
					$markClass = 'text-danger';
					$markIcon = 'fa-arrow-down';
				}
				if (($markPerc < 0) and ($mark > 0.01)) {
					$markClass = 'text-warning';
					$markIcon = 'fa-arrow-down';
				}
				if ($markPerc > 0) {
					$markClass = 'text-success';
					$markIcon = 'fa-arrow-up';
				}
				echo "<div class='col-md-2'>";
				echo "<B >{$incName}</B><HR>";
				echo "<span class='pull-left'><i class='fa {$markClass} {$markIcon} fa-3x'></i></span>";
				echo "<span class=\"{$markClass}\">";
				$markPoint = round($summary[$coin][$inc]['points'], 4);
				echo(($markPoint > 0) ? "+" : "");
				echo $markPoint;
				echo " points <BR>";
				echo(($markPerc > 0) ? "+" : "");
				echo $markPerc;
				echo "%<BR>";
				echo "</span>";
				echo "<span class='clearfix'></span>";
				echo "High : ".$summary[$coin][$inc]['high']."<BR>";
				echo "Low : ".$summary[$coin][$inc]['low']."<BR>";
				echo "</div>";
			}
			echo "</fieldset>";
		}
	}

	function listing($params,$route) {
		$s = Scope::instance();
		$item = new mdb_GIF($params['id']);
		$s->assign('listing',$item);
		echo $s->render("listing");
	}

	function contact($params,$route) {
		$s = Scope::instance();
		$s->assign("sent",false);
		if ($_SERVER['REQUEST_METHOD']  == "POST") {
			$email = strip_tags($_POST['email']);
			$message = strip_tags($_POST['message']);
			$body = "
				Message from : {$email} \n\n
			{$message}\n\n
			";
			$body .= "Sent from GifBPM.com @ ".time()."\n\n";
			mail('dan@danmorgan.net','GIFBPM Contact',$body);
			$s->assign("sent",true);
		}
		echo $s->render("contact");
	}

}
?>

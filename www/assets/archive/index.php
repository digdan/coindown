<?php include("header.php") ?>
<!-- page content -->
<div class="page-content">

    <!-- revolution slider -->
    <div class="banner-container">
        <div class="banner">

            <ul>

                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"  data-saveperformance="on">
                    <img src="assets/images/backgrounds/coindown-bg-1.png" />

                    <div class="tp-caption lft customout rs-parallaxlevel-0"
                         data-x="150"
                         data-y="30" 
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="700"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         style="z-index: 2;">
                        <img src="assets/slider-layers/coindown-title.png" alt="Atlant"/>
                    </div>


                    <div class="tp-caption lft customout rs-parallaxlevel-0"
                         data-x="750"
                         data-y="180" 
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="700"
                         data-start="1200"
                         data-easing="Power3.easeInOut"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/bankingcoins.png" alt="Atlant"/>
                    </div>

                    <div class="tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0"
                         data-x="50"
                         data-y="150" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="1000"
                         data-easing="Back.easeOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         style="z-index: 4;">
                        <img src="assets/slider-layers/infobox1.png" alt="Atlant"/>
                    </div>

                    <div class="tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0"
                         data-x="50"
                         data-y="300" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="1100"
                         data-easing="Back.easeOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         style="z-index: 5;">
                        <img src="assets/slider-layers/infobox2.png" alt="Atlant"/>
                    </div>

                </li>                                    

                <li data-transition="fade" data-slotamount="1" data-masterspeed="700" >
                    <img src="assets/video/video_typing_cover.jpg"  alt="video_typing_cover"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">                                

                    <div class="tp-caption tp-fade fadeout fullscreenvideo"
                         data-x="0"
                         data-y="0"
                         data-speed="1000"
                         data-start="1100"
                         data-easing="Power4.easeOut"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1500"
                         data-endeasing="Power4.easeIn"
                         data-autoplay="true"
                         data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true"
                         data-volume="mute" data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on" style="z-index: 2;">

                        <video class="" preload="none" width="100%" height="100%" poster="assets/video/video_typing_cover.jpg"> 
                            <source src='assets/video/computer_typing.mp4' type='video/mp4'/>
                            <source src='assets/video/computer_typing.webm' type='video/webm'/>
                        </video>
                    </div>

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="280" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="1500"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_1.png"/>
                    </div>                                

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="320" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="1700"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_2.png"/>
                    </div>

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="362" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="1900"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_3.png"/>
                    </div>

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="404" 
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="2100"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_4.png"/>
                    </div>

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="446"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="2200"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_5.png"/>
                    </div>

                    <div class="tp-caption customin ltl"
                         data-x="600"
                         data-y="488"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="500"
                         data-start="2400"
                         data-easing="Power4.easeInOut"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn"
                         style="z-index: 3;">
                        <img src="assets/slider-layers/second_6.png"/>

                    </div>

                </li>
            </ul>

        </div>
    </div>                        
    <!-- ./revolution slider -->                                        

    <!-- page content wrapper -->
    <div class="page-content-wrap bg-texture-1 bg-dark light-elements">

        <div class="divider"><div class="box"><span class="fa fa-angle-down"></span></div></div>

        <!-- page content holder -->
        <div class="page-content-holder">

            <div class="row">

                <div class="col-md-3 col-md-offset-3">                                
                    <div class="text-center">
                        <h3><a href="../html/index.html" class="btn btn-primary btn-xl"><span class="fa fa-bullhorn"></span>Create A Free Alert</a></h3> 
                        <p>No Login Required</p>
                    </div>                             
                </div>

                <div class="col-md-3">                                
                    <div class="text-center">
                        <h3><a href="#" data-toggle="modal" data-target="#AccountLogin" class="btn btn-primary btn-xl"><span class="fa fa fa-gear fa-2x"></span>Manage Your Alerts</a></h3>
                        <p>Premium Users Only</p>
                    </div>                           
                </div>
            </div>
        </div>
	</div>
    <div class="page-content-wrap bg-texture-1 bg-dark light-elements">
	    <div class="page-content-holder">
            <div id="recents" style="background:black;border-radius:10px;padding:20px;">
            </div>
	    </div>
    </div>
<script>
	$(function() {
		function callAjaxRecent(){
			$('#recents').load("/ajaxRecent");
		}
		setInterval(callAjaxRecent, (5*60*60) );
		callAjaxRecent();
	});

</script>

<?php include("footer.php") ?>

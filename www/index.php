<?php
//Initiate Autoloader
include("autoload.php");

$config = Config::instance('default','../config.php');
$scope = Scope::instance('default',$config["paths"]["templates"]);
$router = Router::instance('default', include 'routes.php' );

$mdb = new Mongo($config['dsn']);
$coindown = $mdb->coindown;
DBObject::setDB( $coindown ); //Set Mongo ORM DB Handler

session_start();

try {
    $route = $router->match();
    controllers_Global::main($route['name'],$route['params']);
    if (isset($route)) {
        if ($router->execute( $route ) === FALSE) {
            echo Scope::instance()->render('404');
        }
    } else {
        echo Scope::instance()->render('404');
    }
} catch ( Exception $e ) {
    echo Scope::instance()->render( 'error', array( "error" => $e->getMessage() ) );
}
?>
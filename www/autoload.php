<?php
function autoloader($class,$dir) {
	if (strstr($class,"\\")) $class = array_pop(explode("\\",$class)); //Namespace aware
	if (strstr($class,$dir."_")) $class = array_pop(explode("_",$class)); //Remove prefixing class names : controller_
	if (strstr($class,"_")) {
		$class_path = join("/",explode("_",$class));
		$target = __DIR__."/{$dir}/{$class_path}.php";
	} else {
		$target = __DIR__."/{$dir}/{$class}/{$class}.php";
	}

	if (file_exists($target)) { //Does it have its own subdir?
		include_once($target);
		return true;
	}
	$target = __DIR__."/{$dir}/{$class}.php";
	if (file_exists($target)) { //Or its own file
		include_once($target);
		return true;
	}
	return false;
}

//Register autoloaders
spl_autoload_register( function( $class ) { autoloader( $class , 'libs' ); } ); //Libraries
spl_autoload_register( function( $class ) { autoloader( $class , 'controllers' ); } ); //Libraries
spl_autoload_register( function( $class ) { autoloader( $class , 'mdb' ); } ); //Libraries
?>